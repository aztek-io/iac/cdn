/* Route53 Record for s3 only
resource "aws_route53_record" "s3_bucket" {
    zone_id = data.aws_route53_zone.selected.zone_id
    name    = var.sub_domain
    type    = "A"

    alias {
        name                    = aws_s3_bucket.cdn.website_domain
        zone_id                 = aws_s3_bucket.cdn.hosted_zone_id
        evaluate_target_health  = false
    }
}
*/

resource "aws_route53_record" "cloudfront" {
    zone_id = data.aws_route53_zone.selected.zone_id
    name    = var.sub_domain
    type    = "A"

    alias {
        name                    = aws_cloudfront_distribution.cdn.domain_name
        zone_id                 = aws_cloudfront_distribution.cdn.hosted_zone_id
        evaluate_target_health  = false
    }
}
