resource "aws_acm_certificate" "cert" {
    provider            = aws.dc
    domain_name         = join(".", [var.sub_domain, var.domain])
    validation_method   = "DNS"

    lifecycle {
      create_before_destroy = true
    }
}

resource "aws_route53_record" "cert_validation" {
    name    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
    type    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
    zone_id = data.aws_route53_zone.selected.id
    records = [
        aws_acm_certificate.cert.domain_validation_options.0.resource_record_value
    ]
    ttl     = 60
}

resource "aws_acm_certificate_validation" "cert" {
    provider                = aws.dc
    certificate_arn         = aws_acm_certificate.cert.arn
    validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
}
