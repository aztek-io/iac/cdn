resource "aws_cloudfront_origin_access_identity" "cdn" {}

resource "aws_cloudfront_distribution" "cdn" {
    origin {
        domain_name = aws_s3_bucket.cdn.bucket_regional_domain_name
        origin_id   = aws_cloudfront_origin_access_identity.cdn.id

        s3_origin_config {
          origin_access_identity = "origin-access-identity/cloudfront/${aws_cloudfront_origin_access_identity.cdn.id}"
        }
    }

    enabled             = true
    default_root_object = "index.html"

    custom_error_response {
        error_code          = "404"
        response_code       = "404"
        response_page_path  = "/error.html"
    }

    aliases = [
        join(".", [var.sub_domain, var.domain])
    ]

    viewer_certificate {
        acm_certificate_arn         = aws_acm_certificate.cert.arn
        minimum_protocol_version    = "TLSv1.2_2018"
        ssl_support_method          = "sni-only"
    }

    default_cache_behavior {
        allowed_methods         = ["GET", "HEAD"]
        cached_methods          = ["GET", "HEAD"]
        target_origin_id        = aws_cloudfront_origin_access_identity.cdn.id
        viewer_protocol_policy  = "redirect-to-https"

        forwarded_values {
            query_string = false

            cookies {
                forward = "none"
            }
        }
    }

    restrictions {
        geo_restriction {
            restriction_type = "none"
        }
    }
}
