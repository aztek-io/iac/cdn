terraform {
    backend "s3" {
        bucket  = "aztek.terraform.tfstate"
        key     = "cdn/terraform.tfstate"
        region  = "us-west-2"
        encrypt = "true"
    }
}

