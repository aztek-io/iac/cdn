resource "aws_s3_bucket" "cdn" {
    bucket          = "${var.sub_domain}.${var.domain}"
    acl             = "public-read"
    force_destroy   = true

    website {
        index_document = "index.html"
        error_document = "error.html"
    }
}

